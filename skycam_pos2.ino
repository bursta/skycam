/*
G1 X30.23 Y134.45 Z256.67

(0,y)      (x,y)
  Y---------Z
  |         |
  |         |
  X---------A
(0,0)      (x,0)

*/

//#define DEBUG
//#define TEST

//#define MEGA

#ifndef MEGA
  #define MOTOR_0_DIR_PIN           (7)
  #define MOTOR_0_STEP_PIN          (4)
  
  #define MOTOR_1_DIR_PIN           (6)
  #define MOTOR_1_STEP_PIN          (3)
  
  #define MOTOR_2_DIR_PIN           (5)
  #define MOTOR_2_STEP_PIN          (2)
  
  #define MOTOR_3_DIR_PIN           (13)
  #define MOTOR_3_STEP_PIN          (12)
  
  #define MOTOR_ENABLE_PIN          (8)
#endif

#ifdef MEGA
  #define MOTOR_0_DIR_PIN           (55)
  #define MOTOR_0_STEP_PIN          (54)
  #define MOTOR_0_ENABLE_PIN        (38)
  
  #define MOTOR_1_DIR_PIN           (61)
  #define MOTOR_1_STEP_PIN          (60)
  #define MOTOR_1_ENABLE_PIN        (56)
  
  #define MOTOR_2_DIR_PIN           (48)
  #define MOTOR_2_STEP_PIN          (46)
  #define MOTOR_2_ENABLE_PIN        (62)
  
  #define MOTOR_3_DIR_PIN           (34)
  #define MOTOR_3_STEP_PIN          (36)
  #define MOTOR_3_ENABLE_PIN        (30)
#endif

#define BAUD            (115200)
#define MAX_BUF         (64)

#define TIMER_SPEED 250

#define LENGTH_X 2000.0
#define LENGTH_Y 2000.0
#define LENGTH_Z 3000.0

#define PULLEY_RADIUS 30.0
#define CIRCLE_STEPS 800.0

#define MOTOR_COUNT 4

#define ACC_STEPS 100

byte motorNum = 0;
byte motor[4] = {MOTOR_0_STEP_PIN, MOTOR_1_STEP_PIN, MOTOR_2_STEP_PIN, MOTOR_3_STEP_PIN };
byte motorDir[4] = {MOTOR_0_DIR_PIN, MOTOR_1_DIR_PIN, MOTOR_2_DIR_PIN, MOTOR_3_DIR_PIN };
byte motorAcc[4] = {ACC_STEPS, ACC_STEPS, ACC_STEPS, ACC_STEPS};
byte motorAccCount[4] = {0, 0, 0, 0};
byte motorStepDir[4] = {0, 0, 0, 0};

static char buffer[MAX_BUF];
static int sofar;

float posVector[3] = {0.0, 0.0, 0.0}; // 3D vector of current position

float newLength[4]; // not new length but length diff to current length
float originLength[4]; // hold length of position (0,0,0)

long actualSteps[4] = {0, 0, 0, 0};
int newStepPos[4] = {0, 0, 0, 0};

float limit_xmax = LENGTH_X/2;
float limit_xmin = -LENGTH_X/2;
float limit_ymax = LENGTH_Y/2;
float limit_ymin = -LENGTH_Y/2;
float limit_zmax = LENGTH_Z;
float limit_zmin = -LENGTH_Z;

long counter = 0;

float step_length = (2*PI*PULLEY_RADIUS) / CIRCLE_STEPS;


static void processCommand() {
  #ifdef DEBUG
    Serial.println("Command: " + String(buffer));
    Serial.println("Actual Steps: X: " + String(actualSteps[0]) + " Y: " + String(actualSteps[1]) + " Z: " + String(actualSteps[2]) + " A: " + String(actualSteps[3]));
  #endif
  
  if (!strncmp(buffer,"G0 " ,3)) {
          
    char *ptr=buffer;
    while (ptr && ptr<buffer+sofar) {
      ptr=strchr(ptr,' ')+1;
      
      switch(*ptr) {
        case 'X':
          posVector[0]=atof(ptr+1);
          break;
        case 'Y':
          posVector[1]=atof(ptr+1);
          break;
        case 'Z':
          posVector[2]=atof(ptr+1);
          break;
        case 'F':
          OCR1A = atof(ptr+1);
          break;
        default:
          ptr=0;
          break;
      }
    }

    #ifdef DEBUG
      Serial.println("Input: X: " + String(posVector[0]) + " Y: " + String(posVector[1]) + " Z: " + String(posVector[2]));
    #endif
  }
}

void IK(float nLength[]) {
  float x = posVector[0];
  float y = posVector[1];
  float z = posVector[2];
  
  float dy,dx,dz;

  dz = limit_zmax - z;

  // X
  dy = y - limit_ymin;  dx = x - limit_xmin;
  nLength[0] = sqrt(dx*dx+dy*dy+dz*dz);

  // Y
  dy = y - limit_ymax;  dx = x - limit_xmin;
  nLength[1] = sqrt(dx*dx+dy*dy+dz*dz);

  // Z
  dy = y - limit_ymax;  dx = x - limit_xmax;
  nLength[2] = sqrt(dx*dx+dy*dy+dz*dz);

  // A
  dy = y - limit_ymin;  dx = x - limit_xmax;
  nLength[3] = sqrt(dx*dx+dy*dy+dz*dz);
}

void getStepPos() {
  for (byte i=0; i<MOTOR_COUNT; i++) {
    float tmpStepPos = (newLength[i] - originLength[i]) / step_length;

    newStepPos[i] = tmpStepPos >= 0.0 ? floor(tmpStepPos) : ceil(tmpStepPos);
  }

  #ifdef DEBUG
    Serial.println("New Steps: X: " + String(newStepPos[0]) + " Y: " + String(newStepPos[1]) + " Z: " + String(newStepPos[2]) + " A: " + String(newStepPos[3]));
  #endif
}

void initialize() {
  cli();//stop interrupts
  //Timer Setup:
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A = TIMER_SPEED;// = (16*10^6) / (1*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS10 and CS12 bits for 8 prescaler
  TCCR1B |= (1 << CS11);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  sei();//allow interrupts
  
  pinMode(MOTOR_0_DIR_PIN, OUTPUT);
  pinMode(MOTOR_0_STEP_PIN, OUTPUT);
  digitalWrite(MOTOR_0_DIR_PIN, LOW);
  #ifdef MEGA
    pinMode(MOTOR_0_ENABLE_PIN, OUTPUT);
    digitalWrite(MOTOR_0_ENABLE_PIN, LOW);
  #endif

  pinMode(MOTOR_1_DIR_PIN, OUTPUT);
  pinMode(MOTOR_1_STEP_PIN, OUTPUT);
  digitalWrite(MOTOR_1_DIR_PIN, LOW);
  #ifdef MEGA
    pinMode(MOTOR_1_ENABLE_PIN, OUTPUT);
    digitalWrite(MOTOR_1_ENABLE_PIN, LOW);
  #endif
  
  pinMode(MOTOR_2_DIR_PIN, OUTPUT);
  pinMode(MOTOR_2_STEP_PIN, OUTPUT);
  digitalWrite(MOTOR_2_DIR_PIN, LOW);
  #ifdef MEGA
    pinMode(MOTOR_2_ENABLE_PIN, OUTPUT);
    digitalWrite(MOTOR_2_ENABLE_PIN, LOW);
  #endif
  
  pinMode(MOTOR_3_DIR_PIN, OUTPUT);
  pinMode(MOTOR_3_STEP_PIN, OUTPUT);
  digitalWrite(MOTOR_3_DIR_PIN, LOW);
  #ifdef MEGA
    pinMode(MOTOR_3_ENABLE_PIN, OUTPUT);
    digitalWrite(MOTOR_3_ENABLE_PIN, LOW);
  #endif

  pinMode(MOTOR_ENABLE_PIN, OUTPUT);
  digitalWrite(MOTOR_ENABLE_PIN, LOW);
}

void copy(int* src, int* dst, int len) {
    for (int i = 0; i < len; i++) {
        *dst++ = *src++;
    }
}

void setup() {
  Serial.begin(BAUD);

  #ifndef TEST
    initialize();
  #endif
  
  sofar=0;

  IK(originLength);

  #ifndef DEBUG
    Serial.println("DEBUG OFF");
  #endif
}

void loop() {
  while(Serial.available() > 0) {
      byte incoming = Serial.read();
      if (incoming == '\n') continue;

      buffer[sofar++] = incoming;
      if(buffer[sofar-1]==';') break;
  }
 
  if(sofar>0 && buffer[sofar-1] ==';') {
    buffer[sofar]=0;

    processCommand();

    IK(newLength);

    getStepPos();
    
    sofar=0;
  }

  // counter timer part
  #ifdef TEST

  #endif
}

#ifndef TEST
  ISR(TIMER1_COMPA_vect) {
    int stepsToGo = actualSteps[motorNum] - newStepPos[motorNum];

    if (stepsToGo < 0) {
      if (motorAccCount[motorNum] == abs(motorAcc[motorNum])) {
        digitalWrite(motorDir[motorNum], LOW);
        
        digitalWrite(motor[motorNum], HIGH);
        digitalWrite(motor[motorNum], LOW);
  
        actualSteps[motorNum]++;
        
        if (motorAcc[motorNum] > 0) {
          motorAcc[motorNum]--;
        } else if (abs(stepsToGo) <= ACC_STEPS) {
          motorAcc[motorNum]--;
        }
        
        motorAccCount[motorNum] = 0;
      }
      
      motorAccCount[motorNum]++;
    } else if (stepsToGo > 0) {
      if (motorAccCount[motorNum] == abs(motorAcc[motorNum])) {
        digitalWrite(motorDir[motorNum], HIGH);
        
        digitalWrite(motor[motorNum], HIGH);
        digitalWrite(motor[motorNum], LOW);
  
        actualSteps[motorNum]--;
        
        if (motorAcc[motorNum] > 0) {
          motorAcc[motorNum]--;
        } else if (abs(stepsToGo) <= ACC_STEPS) {
          motorAcc[motorNum]++;
        }
        

        motorAccCount[motorNum] = 0;
      }
      
      motorAccCount[motorNum]++;
    } else {
      motorAccCount[motorNum] = 0;
      motorAcc[motorNum] = ACC_STEPS;
    }

    if (motorNum < (MOTOR_COUNT - 1)) {
      motorNum++;
    } else {
      motorNum = 0;
    }
  }
#endif
